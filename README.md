### For Kolya
1. Прочитай readme.md. Що там треба змінити або дописати??? Перше, що цікавить то опис  Run the project, як йог правильно писати
2. Сам запуск програми з терміналу. Що я їм маю скинути??? бо я на роботі джарку створив і вона запускалась...тобто в помі всі залежності прописав правильно. Але та джарка чомусь до репозиторію не додається(або  не зумів її додати) Як мені поянити їм(описати), що вони мають робити з тою програмою??? вони мають стягнути сам код і задопомогою мавену теж створити джарку??
3. ПОМ - глянь одним оком на нього...чи я все правильнозаписав...бо працювати працює, а от чи все так як має бути))
4. Клас Expense - там, посуті нічого складного немає, тому дуаю, що він правильний
5. Клас ApiUnits - ще до кінця не розібрався як воно працює (бо знайшов приклад, переробив його під свій лад)...але і в мене воно працює))) але глянь, чи така ідея має право на життя...
6. Клас Main -

  * Оголошення і ініціалізація змінних - чи воно так може бути? 
  
  * В методі exit() використовую таку штуку як System.exit(0), бо в мене виникали проблеми з виходом з програми (коли пару раз повертаюсь в menuStar, а потім хочу вийти з нього, то воно декілька раз проходить через метод checkMenu). І я незнаю що з тим зробити, тому використав System.exit(0). Може ти знайдеш в чому проблема.
  
  * Чи варто деякі методи з класу Main перенести в окремий клас Service...Чи можна залишити все так як є?
7. Якщо ти мені не напишеш багато зауважень, то попробую ще завтра написати якісь тести по коду.
8. Ну і вкінці, глянь так взагальному на все підряд...якщо зможеш, то пиши зразу тут а readme, а ні то пиши в скайп))) я все прийму і виправлю завтра)


#Financial_Expenses
## Information

### Project Information
 This project is a personal expenses management console application that allows users to track how much money have they spent.
###If you want to use this project:

1. You have to "clone" on your GitHub or "download" this project;
2. If you selected to "download" the project then start your Intellij IDEA and choose the "Import Project" and then choose as "Maven" project else if you selected to "clone" the project choose the "Check out from Version Control" and then choose "GitHub" and and select your repository in which you cloned this project;
4. Run the project;

### Run the project
You can run a project with IDEA or start with cmd.exe or another terminal.
 
 To do this, you need to download this project and run in terminal file: Financial_Expenses-1.0-SNAPSHOT.jar(folder: Financial_Expenses/terget). Use the command: java -jar Financial_Expenses-1.0-SNAPSHOT 

### Implementation Information
1. Programming Language: Java
2. JDK version: 1.8
3. Build System: Maven
4. Automated Testing: JUnit4
5. API client: Httpclient version 4.5.5

### Contacts
Volodymyr Dykun 

mail: dykun.v.v@gmail.com 

skype: volodymyr298