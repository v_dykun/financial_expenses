import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.*;

public class Main {

   private Map<LocalDate, ArrayList<Expense>> map;
   private Double currencyEur;
   private ApiUtils apiUtils;

    public static void main(String[] args) throws IOException, ParseException {
        Main main = new Main();

        // this start real program
        main.map = new TreeMap<>();
        main.currencyEur=0.0;
        main.apiUtils = new ApiUtils();

        // only for testing code. After testing delate method test();
        main.test();
        main.menuStart();

    }

    private void menuStart() {
        System.out.println("Enter the command:");
        BufferedReader reader = new BufferedReader( new InputStreamReader(System.in));
        try {
         /*
         we break the entered string to pull out the command and the date
         arr[0] - its command
         arr[1] - its date (if it is needed)
         arr[2] - its price (if it is needed)
         arr[3] - its currency (if it is needed)
         arr[4] and next - its name (if it is needed)
           */
         String insert = reader.readLine();
         String [] arr = insert.split(" ");

            switch(arr[0]) {
                case "add":
                    menuAdd(arr);
                    checkMenu();
                    break;
                case "list":
                    menuList(arr);
                    checkMenu();
                    break;
                case "clear":
                    menuClear(arr);
                    checkMenu();
                    break;
                case "total":
                    menuTotal(arr);
                    checkMenu();
                    break;
                case "exit":
                    menuExit();
                    break;
                default:
                    System.out.println("There is no such command, please, try again!!");
                    menuStart();
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private LocalDate checkDate (String date) {
        //this method parsed date from String to LocalDate
       try{
           return LocalDate.parse(date);
       } catch (DateTimeParseException e){
           // if dates format unreal, we become to start menu
           System.out.println("You entered the wrong date, please, try again!!");
           menuStart();
           return null;
       }
    }

    private void menuAdd (String[] arr) throws IOException {
        Expense expense = new Expense();
        expense.setDate(checkDate(arr[1]));
        expense.setPrice(Double.parseDouble(arr[2]));
        expense.setCurrency(arr[3].toUpperCase());

        // add all words after arr[3] in name
        String name=arr[4];
        for (int i=5; i<arr.length;i++) {
            name = name+" "+arr[i];
        }
        expense.setName(name);
        add(expense);
    }
    private void menuList (String[] arr) throws IOException {
        /*
        * command "list" should consist of one word, so we check this and print list expense
        */
        if (arr.length==1) {
            printMap();
        } else {
            System.out.println("The command \"List\" should contain only one word, please, try again!!");
            menuStart();
        }

    }
    private void menuClear (String[] arr) throws IOException {
        clear(arr[1]);
    }
    private void menuTotal (String[] arr) throws IOException {
        try {
            total(arr[1].toUpperCase());
        } catch (ArrayIndexOutOfBoundsException e){
            System.out.println("You have not entered the currency, please, try again!! ");
            menuStart();
        }
    }
    private void menuExit() {
        System.out.println("Thank you for using our program.");
        System.exit(0);
    }

    private void checkMenu() {
        System.out.println("Want to continue?");
        System.out.println("YES - enter \"Y\"");
        System.out.println("NO - enter \"N\"");
        BufferedReader reader = new BufferedReader( new InputStreamReader(System.in));
        /*
        * The method checks if you want to leave or stay
        * if you want leave - call menuExit
        * if you want stay - call menuStart
        */
        try {
            String insert = reader.readLine();
            if (insert.toUpperCase().equals("Y")){
                menuStart();
            }  else if (insert.toUpperCase().equals("N")){
                menuExit();
            } else {
                System.out.println("You have entered the wrong answer, try again.");
                checkMenu();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void test() throws IOException{
        /*this methon only for testing program
        * delete this method after testing
        */

        Expense expense1 = new Expense("2015-10-11", 21d, "USD","w1");
        Expense expense2 = new Expense("2015-10-12", 22d, "EUR","w2");
        Expense expense3 = new Expense("2015-10-14", 23d, "USD","w3");
        Expense expense4 = new Expense("2015-10-15", 24d, "UAH","w4");
        Expense expense5 = new Expense("2015-10-12", 25d, "PLN","w5");
        Expense expense6 = new Expense("2015-10-15", 26d, "EUR","w6");

        add(expense1);
        add(expense2);
        add(expense3);
        add(expense4);
        add(expense5);
        add(expense6);
    }

    private void add(Expense expense) throws IOException {
        /*this method checks for the date in the array
         * if this date is present - we add to this date new expense
          * if this date is absent - we add new date with expense*/
        for( Map.Entry<LocalDate, ArrayList<Expense>> entry : map.entrySet() ){
            if (entry.getKey().equals(expense.getDate())) {
               map.get(expense.getDate()).add(expense);
                currencyEur=currencyEur+apiUtils.convertCurrencyToEur(expense.getPrice(),expense.getCurrency());
                printMap();
               return;
            }
        }
        ArrayList<Expense> list = new ArrayList<>();
        list.add(expense);
        map.put(expense.getDate(),list);
        currencyEur=currencyEur+ apiUtils.convertCurrencyToEur(expense.getPrice(),expense.getCurrency());
       printMap();
    }

    private void printMap() {
        System.out.println("New expense:");
        for (Map.Entry<LocalDate, ArrayList<Expense>> entry : map.entrySet())
        {
            System.out.println(entry.getKey());
            printList(entry.getValue());

        }
    }

    private void printList(ArrayList<Expense> list) {
     for (int i=0; i<list.size();i++) {
         System.out.println(list.get(i).getName()+" "+list.get(i).getPrice()+" "+list.get(i).getCurrency());
     }
        System.out.println();
    }

    private void clear(String date) {
        // if you have not entered more expenses
        if (map.size()==0){
            System.out.println("You don't have any more expenses!");
            System.out.println();
            checkMenu();
        }

        // if you have entered at least one expense
        for( Map.Entry<LocalDate, ArrayList<Expense>> entry : map.entrySet() ){
            if (entry.getKey().equals(checkDate(date))) {

                for (int i=0;i<entry.getValue().size();i++) {
                    currencyEur=currencyEur-apiUtils.convertCurrencyToEur(entry.getValue().get(i).getPrice(),entry.getValue().get(i).getCurrency());
                }
                System.out.println("Map after remote:");
                map.remove(checkDate(date));
                printMap();
                return;

            }
        }
    }

    private void total(String currency){
        /*
        after all convertCurrencyToEur we convert currencyEur to the chosen currency
        */
      try {
          Double coef;
          if (!currency.equals("EUR")) {
              coef= apiUtils.parseCurrentApiJson(currency);
          } else coef=1.0;
          Double total = currencyEur*coef;
          String formattedTotal = new DecimalFormat("#0.00").format(total);
          System.out.println(formattedTotal+" "+currency );
      } catch (NullPointerException e) {
          System.out.println("No currency with this name was found, please, try again!!");
          menuStart();
      }

    }
}
